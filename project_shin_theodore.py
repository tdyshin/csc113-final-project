# Theodore Shin
# CSc 11300 Afternoon Section
# Project
# Tested with Python 3.4.3

from tkinter import *
import tkinter
from turtle import *
import turtle
import operator

class Application(Frame):
    def __init__(self, master=None):
        tkinter.Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

    def createWidgets(self):

        # window title
        root.title("Fequent Letters")

        # Input box text
        textFrame = Frame(self)
        entryLabel = Label(textFrame)
        entryLabel["text"] = "Enter n:"
        entryLabel.pack(side="left")

        # input box
        self.entryWidget = Entry(textFrame)
        self.entryWidget["width"] = 15
        self.entryWidget.insert(0, "3")
        self.entryWidget.pack(side="left")
        textFrame.pack()

        button = Button(self, text="Submit", command=self.drawChart)
        button.pack(side="top")

        # canvas for drawing pie chart
        canvasFrame = Frame(self)
        self.canvas = Canvas(canvasFrame, width=600, height=600)
        self.canvas.pack()
        canvasFrame.pack()

        # Draw default chart with n = 3
        self.drawChart()

        # quit button
        self.QUIT = Button(self)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"] = "red"
        self.QUIT["command"] = self.quit
        self.QUIT.pack(side="bottom")

    # read file contents
    def readFile(self):
        fo = open('Words.txt', 'r')
        contents = fo.read()
        fo.close()
        return contents

    def getProbabilities(self, n, contents):
        charCounts = {}
        totalChars = len(contents)
        for i in contents:
            if i in charCounts:
                charCounts[i] += 1
            else:
                charCounts[i] = 1

        for key, value in charCounts.items():
            charCounts[key] = round((value / totalChars), 4)

        sortedCharCounts = sorted(charCounts.items(), key=operator.itemgetter(1), reverse=TRUE)
        firstNLetters = sortedCharCounts[:n]
        numUsedCharsProb = 0
        for i in firstNLetters:
            numUsedCharsProb += i[1]
        # tuple1 = ('All Other Letters', round(((totalChars - round(numUsedCharsProb*totalChars)) / totalChars), 4 ))
        tuple1 = ('All Other Letters', 1 - numUsedCharsProb)
        firstNLetters.append(tuple1)

        return firstNLetters

    def drawChart(self):
        # Clear canvas before drawing anything
        self.canvas.delete("all")
        n = int(self.entryWidget.get())
        contents = self.readFile()
        probArray = self.getProbabilities(n, contents)
        # print(probArray)
        
        # radius of circle
        radius = 200
        yShift = -200

        # draw circle
        turtleCircle = turtle.RawTurtle(self.canvas)
        turtleCircle.speed(10)
        turtleCircle.hideturtle()
        turtleCircle.penup()
        turtleCircle.sety(yShift)
        turtleCircle.pendown()
        turtleCircle.circle(radius)

        angle = 0

        # Draw horizontal line to the right
        testTurtle = turtle.RawTurtle(self.canvas)
        testTurtle.speed(10)
        testTurtle.hideturtle()
        testTurtle.penup()
        testTurtle.sety(yShift + radius)
        testTurtle.pendown()
        testTurtle.setheading(angle)
        testTurtle.forward(radius)

        # Draw lines going out from center to circle
        for i in range(len(probArray)):
            angle += probArray[i][1] * 360
            testTurtle = turtle.RawTurtle(self.canvas)
            testTurtle.speed(10)
            testTurtle.hideturtle()
            testTurtle.penup()
            testTurtle.sety(yShift + radius)
            testTurtle.pendown()
            testTurtle.setheading(angle)
            testTurtle.color("black", "red")
            testTurtle.begin_fill()
            testTurtle.forward(radius)
            # testTurtle.circle(angle, angle)
            testTurtle.end_fill()

            labelTurtle = turtle.RawTurtle(self.canvas)
            labelTurtle.speed(10)
            labelTurtle.hideturtle()
            labelTurtle.penup()
            labelTurtle.sety(yShift + radius)
            labelTurtle.setheading(angle - (probArray[i][1] * 360) / 2)
            labelTurtle.forward(radius * 1.1)
            letter = probArray[i][0]
            prob = probArray[i][1]
            if letter == ' ':
                letter = 'space'
            labelText = letter + " = " + str(prob)
            labelTurtle.write(labelText, True, align="center")


    # test function for printing n*2 in a message box
    def displayN(self):
        n = int(self.entryWidget.get())
        tkinter.messagebox.showinfo("Tkinter Entry Widget", "n = " + str(n*2))


root = tkinter.Tk()
app = Application(master=root)
app.mainloop()